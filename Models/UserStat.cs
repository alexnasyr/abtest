﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

#pragma warning disable IDE1006

namespace ABtest.Models {
    public class UserStat {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long UserID { get; set; }
        public DateTime Registered { get; set; }
        public DateTime LastActivity { get; set; }
    }

    public interface IUserRepository {
        IEnumerable<UserStat> UserStats { get; }
        int dbInsert(UserStat entity);
        int dbDelete(UserStat entity);
    }
}
