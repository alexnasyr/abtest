﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABtest.Models {
    public class DataContext : DbContext {
        public DbSet<UserStat> Users { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=ABtest;Username=postgres;Password=welc0me");
            //optionsBuilder.UseNpgsql("Host=128.65.51.5;Port=5432;Database=ABtest;Username=postgres;Password=welc0me");
        }
    }
}
