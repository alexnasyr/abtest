﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABtest.Models {
    public class ABRepository : IUserRepository {
        private readonly DataContext context;
        public ABRepository() {
            context = new DataContext(); 
        }
        public IEnumerable<UserStat> UserStats => context.Users;
        public int dbInsert(UserStat entity) {
            context.Users.Add(entity);
            return context.SaveChanges();
        }
        public int dbDelete(UserStat entity) {
            context.Users.Remove(context.Users.Where(u=>u.UserID==1).FirstOrDefault());
            return context.SaveChanges();
        }
    }
}
