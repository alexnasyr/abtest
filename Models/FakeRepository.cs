﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABtest.Models {
    public class FakeRepository : IUserRepository {
        public IEnumerable<UserStat> UserStats => new List<UserStat> {
            new UserStat { UserID = 1, Registered = new DateTime(2020, 1, 1), LastActivity = new DateTime(2020, 1, 10) },
            new UserStat { UserID = 2, Registered = new DateTime(2020, 2, 1), LastActivity = new DateTime(2020, 2, 10) },
            new UserStat { UserID = 3, Registered = new DateTime(2020, 3, 1), LastActivity = new DateTime(2020, 3, 10) }
        }.AsQueryable<UserStat>();

        public int dbInsert(UserStat entity) {
            return 0;
        }
        public int dbDelete(UserStat entity) {
            return 0;
        }
    }
}
